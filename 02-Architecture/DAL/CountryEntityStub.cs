﻿namespace DAL
{
    public class CountryEntityStub
    {
        public CountryEntity Get()
        {
            return new CountryEntity { Name = "Украина" };
        }
    }
}
