﻿using _02.Interfaces;

namespace _02.Models
{
    public class CountryModel : ICountryModel
    {
        public string Name { get; set; }
    }
}
