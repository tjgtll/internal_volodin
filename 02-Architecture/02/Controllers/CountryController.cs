﻿using Microsoft.AspNetCore.Mvc;
using BL;
using _02.Interfaces;

namespace _02.Controllers
{
    public class CountryController : Controller
    {
        private readonly ICountryModel countryModel;
        private readonly ICountryManager countryManager;

        public CountryController(ICountryModel countryModel, ICountryManager countryManager)
        {
            this.countryModel = countryModel;
            this.countryManager = countryManager;
        }

        public IActionResult Index()
        {
            var manager = countryManager;
            var model = countryModel;

            model.Name = $"{manager.Get().Name}PL";

            return Content(model.Name);
        }
    } 
}   