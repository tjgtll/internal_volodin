﻿namespace _02.Interfaces
{
    public interface ICountryModel
    {
        public string Name { get; set; }
    }
}
