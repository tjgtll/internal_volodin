﻿using DAL;

namespace BL
{
    public class CountryManager : ICountryManager
    {
        public Country Get()
        {
            var countryEntityStub = new CountryEntityStub();
            var countryEntity = countryEntityStub.Get();

            return new Country { Name = $"{countryEntity.Name}BL" };
        }
    }
}
