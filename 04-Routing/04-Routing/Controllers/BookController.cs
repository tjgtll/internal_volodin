﻿using Microsoft.AspNetCore.Mvc;
using System;

namespace _04_Routing.Controllers
{
    public class BookController : Controller
    {
        private const string _errorMessageIdMoreThanGiven = "Error: number more than given.";
        private const int _limitId = 10;

        public IActionResult Index(int bookId)
        {
            if (bookId <= _limitId) return Content(bookId.ToString());
            return Content(_errorMessageIdMoreThanGiven);
        }
    }
}
