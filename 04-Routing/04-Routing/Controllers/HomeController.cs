﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using _04_Routing.Models;

namespace _04_Routing.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Custom()
        {
            return Content("Custom");
        }
    }
}
