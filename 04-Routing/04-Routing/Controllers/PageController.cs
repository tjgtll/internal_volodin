﻿using Microsoft.AspNetCore.Mvc;
using System;

namespace _04_Routing.Controllers
{
    public class PageController : Controller
    {
        private const string _errorMessageStringLenght = "Error: string length is greater.";
        private const int _limitStringLenght = 5;

        [HttpGet("pages/{pageName}")]
        [HttpGet("sheets/{pageName}")]
        public IActionResult Get(string pageName)
        {
            if (pageName.Length<= _limitStringLenght) return Content(pageName);
            return Content(_errorMessageStringLenght);
        }
    }
}