﻿using Microsoft.Extensions.Configuration;

namespace _03
{
    public interface IRequestСheckManager
    {
         bool СheckRequestLengthSize(string request, IConfiguration Configuration);

         bool CheckRequestСharactersContain(string request, IConfiguration Configuration);
    }
}
