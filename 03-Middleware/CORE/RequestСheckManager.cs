﻿using Microsoft.Extensions.Configuration;
using System.Linq;

namespace _03
{
    public class RequestСheckManager : IRequestСheckManager
    {
        public bool СheckRequestLengthSize(string request, IConfiguration Configuration)
        {
            var requestLenghtOptions = new RequestLenghtOptions();

            Configuration.GetSection(RequestLenghtOptions.RequestLenght).Bind(requestLenghtOptions);

            return (request.Length > requestLenghtOptions.Lenght);
        }

        public bool CheckRequestСharactersContain(string request, IConfiguration Configuration)
        {
            var charactersOptions = new CharactersOptions();

            Configuration.GetSection(CharactersOptions.Characters).Bind(charactersOptions);

            return (request.Any(r => charactersOptions.List.Contains(r)));
        }
    }
}
