﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace _03
{
    public class CheckСharactersMiddleware
    {
        private readonly IConfiguration Configuration;

        private readonly IRequestСheckManager RequestСheckManager;

        private readonly RequestDelegate _next;

        public CheckСharactersMiddleware(RequestDelegate next, IConfiguration configuration, IRequestСheckManager requestСheckManager)
        {
            _next = next;
            Configuration = configuration;
            RequestСheckManager = requestСheckManager;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            var requestСheck = RequestСheckManager;
            //context.Request.QueryString.ToString() == null??????
            if (requestСheck.CheckRequestСharactersContain(context.Request.GetDisplayUrl(), Configuration))
            {
                context.Response.StatusCode = 400;
                await context.Response.WriteAsync($"StatusCode: {context.Response.StatusCode}\nError: request contains characters");
            }
            else
            {
                await _next(context);
            }

        }
    }
}
