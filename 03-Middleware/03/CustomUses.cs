﻿using Microsoft.AspNetCore.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace _03
{
    public static class CustomUses
    {
        public static IApplicationBuilder UseСheckRequestLengthSize(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<СheckRequestLengthSizeMiddleware>();
        }

        public static IApplicationBuilder UseСheckCharacters(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<CheckСharactersMiddleware>();
        }
    }
}
