﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace _05.Controllers
{
    public class SessionController : Controller
    {
        private readonly ISessionService _sessionService;

        public SessionController(ISessionService sessionService)
        {
            _sessionService = sessionService;
        }

        [HttpGet("get")]
        public IActionResult GetAll()
        {
            return Content(_sessionService.GetAll());
        }

        [HttpGet("get/{key}")]
        public IActionResult Get(string key)
        {
            return Content(_sessionService.GetAll());
        }

        [HttpGet("set/{key}/{value}")]
        public IActionResult Set(string key, string value)
        {
            return Content(_sessionService.Set(key, value));
        }

        [HttpGet("delete/{key}")]
        public IActionResult Delete(string key)
        {
            return Content(_sessionService.Delete(key));
        }
    }
}