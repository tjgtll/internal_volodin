using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace _05
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();

            services.AddHttpContextAccessor();

            services.AddTransient<ISessionService, SessionService>();

            services.AddDistributedMemoryCache();

            services.AddSession(options =>
            {
                var sessionOptions = Configuration.GetSection(SessionOptions.Session).Get<SessionOptions>();
                options.Cookie.Name = sessionOptions.Name;
                options.IdleTimeout = TimeSpan.FromSeconds(Convert.ToInt32(sessionOptions.IdleTimeout));
                options.Cookie.IsEssential = Convert.ToBoolean(sessionOptions.IsEssential);
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseRouting();

            app.UseSession();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
