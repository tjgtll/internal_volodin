﻿using Microsoft.AspNetCore.Http;
using System;
using System.Text;

namespace _05
{
    public class SessionService : ISessionService
    {
        private const string _errorMessageNull = "null";

        private const string _errorMessageKeyAlreadySet = "key already set";

        private const string _errorMessageKeyNotSet = "key not set";

        private const string _outMessageSet = "{0} set";

        private const string _outMessageDelete = "{0} delete";

        private readonly IHttpContextAccessor _httpContextAccessor;

        public SessionService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public string GetAll()
        {
            string outStr="";
            var keys = _httpContextAccessor.HttpContext.Session.Keys;
            foreach (var key in keys)
            {
                _httpContextAccessor.HttpContext.Session.TryGetValue(key, out byte[] valueByte);
                outStr += String.Concat(key, " ", Encoding.Default.GetString(valueByte), "\n");
            }
            if (outStr == "") return _errorMessageNull;
            return outStr;
        }

        public string Get(string key)
        {
            if (_httpContextAccessor.HttpContext.Session.TryGetValue(key, out byte[] valueByte))
            {
                return Encoding.Default.GetString(valueByte);
            }
            return _errorMessageKeyNotSet;
        }

        public string Set(string key, string value)
        {
            if (_httpContextAccessor.HttpContext.Session.TryGetValue(key, out byte[] valueByte))
            {
                return _errorMessageKeyAlreadySet;
            } 
            _httpContextAccessor.HttpContext.Session.Set(key, Encoding.UTF8.GetBytes(value));
            return string.Format(_outMessageSet, key);
        }

        public string Delete(string key)
        {
            if (_httpContextAccessor.HttpContext.Session.TryGetValue(key, out byte[] valueByte))
            {
                _httpContextAccessor.HttpContext.Session.Remove(key);
                return string.Format(_outMessageDelete, key);
            }
            return _errorMessageKeyNotSet;
        }
    }
}
