﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace _05
{
    public interface ISessionService
    {
        string GetAll();

        string Get(string key);

        string Set(string key, string value);

        string Delete(string key);
    }
}
