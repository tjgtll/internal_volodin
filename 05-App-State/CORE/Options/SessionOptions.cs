﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace _05
{
    public class SessionOptions
    {
        public const string Session = "Session";

        public string Name { get; set; }
        public string IdleTimeout { get; set; }
        public string IsEssential { get; set; }
    }
}
